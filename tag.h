#include<QtCore>
#ifndef TAG_H
#define TAG_H

class Tag {
 public:
  Tag(): tag_name("") {}
  void getAtrribs();
  void add(QString, QString);
  friend class HTMLParser;
 private:
  QString tag_name;
  QVector<QString> childs;
  QMap<QString, QString> attribs;
};

#endif // TAG_H
