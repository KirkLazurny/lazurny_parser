#include "parser.h"
#include<QtCore>
void HTMLParser::loadHTMLFile(QString path)
{
    QFile HTMLFile;
    HTMLFile.setFileName(path);
    HTMLFile.open(QIODevice::ReadOnly);
    if(HTMLFile.isOpen())
    {

        HTML = HTMLFile.readAll();
    }
    HTMLFile.close();
}


QString HTMLParser::getHTML()
{
    return HTML;
}

void HTMLParser::getTags()
{
    QVector<Tag>::iterator iter = tags.begin();
    while(iter != tags.end())
    {
        QMap<QString, QString>::iterator it = iter->attribs.begin();
        while(it != iter->attribs.end())
        {
            qDebug() << it.key() << "=" << it.value() << "\n";
            ++it;
        }
        ++iter;
    }
}

void HTMLParser::replaceEnters()
{
    HTML.replace("\r\n", " ");
}

void HTMLParser::delTabs()
{
    QStringList words = HTML.split(" ");
    QStringList::iterator strings = words.begin();
    while(strings != words.end())
    {
        if(*strings == "") words.erase(strings);

        if(*strings != "") ++strings;
    }

    HTML = words.join(" ");
}

void HTMLParser::lexical_parsing()
{
    QStringList list = HTML.split(" ");
    Tag New_Tag;
    for (int i=0; i<list.size(); i++)
    {
        bool b1 = (list[i]=="<p>");
        bool b2 = (list[i]=="<a");
        bool b3 = (list[i]=="<img");

        if (b1==true)
        {
            New_Tag.tag_name="p";
            New_Tag.attribs[list[i]]=list[i+1];

        }

        if (b2==true)
        {
            New_Tag.tag_name="a";
            New_Tag.attribs[list[i+1]]=list[i+3];

        }

        if (b3==true)
        {
            New_Tag.tag_name="img";
            New_Tag.attribs[list[i+1]]=list[i+3];
        }
    }
}




